# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    max_val = values[0]

    for num in values:
        if num > max_val:
            max_val = num

    return max_val
