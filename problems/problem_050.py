# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(single_list):
    half = len(single_list) // 2
    first_half = single_list[0:half + 1]
    second_half = single_list[half + 1:len(single_list)]
    return (first_half, second_half)

print(halve_the_list([1, 2, 3, 4]))
